import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.lightBlue,
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text('Menu Utama'),
          centerTitle: true,
          leading: Icon(
            Icons.shopping_cart,
            color: Colors.blue,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.menu_open),
              color: Colors.blue,
              onPressed: () {},
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Picture(),
              TextName(),
              TextName(),
              FirstRow(),
              SecondRow(),
            ],
          ),
        ),
      ),
    );
  }
}

class Picture extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      margin: const EdgeInsets.only(top: 30),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50.50),
        image: DecorationImage(
          image: NetworkImage("https://i.ibb.co/X37Gj88/Whats-App-Image-2020-11-21-at-19-01-11.jpg"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
class TextName extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("Happy Shopping", style: TextStyle(fontSize: 20, color: Colors.white),),
    );
  }
}
class FirstRow extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.orange,
              blurRadius: 10,
              spreadRadius: 1,
            ),
            ]),
            width: 150,
            margin: const EdgeInsets.only(top: 10),
            child: Card(
              child: Padding(
                padding: EdgeInsets.all(18),
                child: Column(
                      children: <Widget>[
                               Icon(Icons.map,
                               size: 40,
                               color: Colors.orange,),
                      Text(
                        'Location', style: TextStyle(color: Colors.black12, fontSize: 20),
                      ),
                      ],
                ),
              ),
            ),
        ),
        Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.orange,
              blurRadius: 10,
              spreadRadius: 1,
            ),
          ]),
          width: 150,
          margin: const EdgeInsets.only(top: 10),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18),
              child: Column(
                children: <Widget>[
                  Icon(Icons.add_business,
                    size: 40,
                    color: Colors.lightBlue,),
                  Text(
                    'Product', style: TextStyle(color: Colors.black12, fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
class SecondRow extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.orange,
              blurRadius: 10,
              spreadRadius: 1,
            ),
          ]),
          width: 150,
          margin: const EdgeInsets.only(top: 10),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18),
              child: Column(
                children: <Widget>[
                  Icon(Icons.add_ic_call,
                    size: 40,
                    color: Colors.lightGreenAccent,),
                  Text(
                    'Contact Us', style: TextStyle(color: Colors.black12, fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.orange,
              blurRadius: 10,
              spreadRadius: 1,
            ),
          ]),
          width: 150,
          margin: const EdgeInsets.only(top: 10),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18),
              child: Column(
                children: <Widget>[
                  Icon(Icons.auto_fix_high,
                    size: 40,
                    color: Colors.amber,),
                  Text(
                    'Tips and Trick', style: TextStyle(color: Colors.black12, fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

